void setup() //called on start
{
  size(1600, 900, P3D);
  camera(0, 0, -(height/2) / tan(PI * 30 / 180), 
        0, 0, 0,
        0, -1, 0);
        
  background(130);
}

int frame = 0;

void drawPaintSplatter()
{
  float sizeSd = 25;
  float sizeMean = 10;
  float sizeGaus = randomGaussian();
  float size = ( sizeGaus * sizeSd ) + sizeMean;
  
  float xGaus = randomGaussian();
  float yGaus = randomGaussian();
  float xSd = width/8;               
  float ySd = height/8;
  float posMean = 0;    
  
  float x = ( xGaus * xSd ) + posMean;
  float y = ( yGaus * ySd ) + posMean; 

  noStroke();
  fill(random(255), random(255), random(255), 100);
  circle(x, y, size);
  
  frame++;
  if (frame == 100)
  {
    frame = 0;
    background(130);
  } 
}

void draw() // called per frame
{ 
  drawPaintSplatter();
}
