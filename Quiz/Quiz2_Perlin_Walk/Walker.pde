class Walker
{
  float xPos;
  float yPos;
  float dtx = 0;
  float dty = 1;
  float dtRed = 200;
  float dtGreen = 150;
  float dtBlue = 305;
  float dtSize = 500;
  
  Walker() //default
  {
    xPos = 0;
    yPos = 0;
  }
  
  Walker(float x, float y)
  {
    xPos = x;
    yPos = y;
  }
  
  void render()
  {
    float redNoise = noise(dtRed);
    float greenNoise = noise(dtGreen);
    float blueNoise = noise (dtBlue);
    float sizeNoise = noise (dtSize);
    
    float redMap = map(redNoise, 0, 1, 100, 255);
    float greenMap = map(greenNoise, 0, 1, 100, 255);
    float blueMap = map(blueNoise, 0, 1, 100, 255);
    float sizeMap = map(sizeNoise, 0, 1, 10, 100);
    
    fill(redMap, greenMap, blueMap);
    circle(xPos, yPos, sizeMap);
   
    dtSize += 0.01f;
    dtRed += 0.01f;
    dtGreen += 0.01f;
    dtBlue += 0.01f;
  }
  
  void walk()
  {
   float xNoise = noise(dtx);
   float yNoise = noise(dty);
   float x = map(xNoise, 0, 1, -width/2, width/2);
   float y = map(yNoise, 0, 1, -height/2, height/2);
   noStroke();
   
   xPos = x;
   yPos = y;
   
   dtx += 0.01f;
   dty += 0.01f;
  }
}
