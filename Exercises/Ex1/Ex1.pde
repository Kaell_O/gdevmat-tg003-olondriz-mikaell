void setup() //called on start
{
  size(1600, 900, P3D);
  camera(0, 0, -(height/2) / tan(PI * 30 / 180), 
        0, 0, 0,
        0, -1, 0);
}

void draw() // called per frame
{
  background(255);
  drawCartesianPlane();
  //drawLinearFunction();
  //drawQuadraticFunction();
  //drawCircle();
  drawSinWave();
}

void drawCartesianPlane()
{
  line(-300, 0, 300, 0);
  line(0, 300, 0, -300);
  for (int i = -300; i <= 300; i += 10)
  {
    line(i, -5, i, 5);
    line(-5, i, 5, i);
  }
}

void drawLinearFunction()
{
  /*
      f(x) = x + 2;
      let x = 2;
      y = 4, (2, 4);
  */
  
  for (int x = -200; x <= 200; x++)
  {
    circle(x, x+2, 1);
  }
}

void drawQuadraticFunction()
{
  /*
      f(x) = x^2 + 2x - 5;
      let x = 2 then y = 3;
      let x = -1 then y = -6;
  */
  
  for (float x = -300; x <= 300; x += 0.1)
  {
    circle(x*10, (x*x) + (2*x) - 5, 1);
  }
}

float radius = 50;

void drawCircle()
{
  for (int x = 0; x < 360; x++)
  {
    circle((float)Math.cos(x) * radius, (float)Math.sin(x) * radius, (float)1);
  }
}

float frequency = 5;
float amplitude = 40;
float xCoord = 0;

void drawSinWave()
{
  //reference: https://processing.org/reference/sin_.html
  float a = 0.0;
  float inc = TWO_PI/25.0;
 
  for (int i = -width; i <= width; i+=frequency) 
  {
    circle(i+xCoord, sin(a)*amplitude, 1);
    a = a + inc;
  }
  xCoord--;
  if (keyPressed)
  {
    if (key == 'd')
    {
      frequency++;
    }
  
    if (key == 'w')
    {
      amplitude++;
    }
    
    if (key == 'a')
    {
      frequency--;
    }
  
    if (key == 's')
    {
      amplitude--;
    }
  }
 
 if (frequency <= 1)
 {
   frequency = 1;
 }
}
