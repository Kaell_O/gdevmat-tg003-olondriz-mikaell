class Walker
{
  float xPos;
  float yPos;
  
  Walker() //default
  {
    xPos = 0;
    yPos = 0;
  }
  
  Walker(float x, float y)
  {
    xPos = x;
    yPos = y;
  }
  
  void render()
  {
    circle(xPos, yPos, 30);
  }
  
  void walk()
  {
    int direction = floor(random(8));
    
    fill(random(255), random(255), random(255));
    switch(direction)
    {
      case 0:
      {
        yPos++;
        break;
      }
      case 1:
      {
        xPos++;
        yPos++;
        break;
      }
      case 2:
      {
        xPos++;
        break;
      }
      case 3:
      {
        xPos++;
        yPos--;
        break;
      }
      case 4:
      {
        yPos--;
        break;
      }
      case 5:
      {
        xPos--;
        yPos--;
        break;
      }
      case 6:
      {
        xPos--;
        break;
      }
      case 7:
      {
        xPos--;
        yPos++;
        break;
      }
      
    }
  }
}
