void setup() //called on start
{
  size(1600, 900, P3D);
  camera(0, 0, -(height/2) / tan(PI * 30 / 180), 
        0, 0, 0,
        0, -1, 0);
}

Walker walker = new Walker();

void draw() // called per frame
{ 
  background(130);
  walker.render();
  walker.walk();
}
