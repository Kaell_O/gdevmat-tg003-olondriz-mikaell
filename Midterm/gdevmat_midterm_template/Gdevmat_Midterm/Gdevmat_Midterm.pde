Mover blackhole = new Mover(0, 0, 50);
Mover[] planet = new Mover[100];
float dtRed = 200;
float dtGreen = 150;
float dtBlue = 305;
int frame = 0;

void setup()
{
  size(1600, 900, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  spawnPlanets();
}

void spawnPlanets()
{
  blackhole.position = new PVector(random(Window.left, Window.right), random(Window.bottom, Window.top));
    for (int i = 0; i < 100; i++)
    {
      float xGaus = randomGaussian();
      float yGaus = randomGaussian();
      float xSd = Window.windowWidth / 2;               
      float ySd = Window.windowHeight / 2;
      float posMean = 0;    
    
      float redMap = map(noise(dtRed), 0, 1, 100, 255);
      float greenMap = map(noise(dtGreen), 0, 1, 100, 255);
      float blueMap = map(noise(dtBlue), 0, 1, 100, 255);
      
      float x = ( xGaus * xSd ) + posMean;
      float y = ( yGaus * ySd ) + posMean; 
    
      planet[i] = new Mover(new PVector(x, y), random(10, 40));
      planet[i].setColor(redMap, greenMap, blueMap);
      noStroke();
      
      dtRed += 0.5f;
      dtGreen += 0.5f;
      dtBlue += 0.5f;
    }
}

void draw()
{
  background(30);

  if (frame >= 130)
  {
    frame = 0;
    spawnPlanets();
  }
  
  for (int i=0; i < planet.length; i++) 
  { 
      planet[i].move(blackhole.position);   
      planet[i].render();
  }
  
  blackhole.setColor(255, 255, 255);
  blackhole.render();
  frame++;
}
